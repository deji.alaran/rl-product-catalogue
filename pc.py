import redis
import os
import argparse

CATALOGUE_COUNTER = "catalogue:counter"
PRODUCT_COUNTER = "product:counter"
IMAGE_COUNTER = "image:counter"

ALL_PRODUCTS="catalogues:all:products"

rc = redis.Redis(
    host=os.environ["REDIS_HOST"],
    port=os.environ["REDIS_PORT"])

def create_catalogue(name):
    id = get_counter(CATALOGUE_COUNTER)
    rc.hmset(cat_key(id),{ "id": id, "name": name })
    return rc.hgetall(cat_key(id))

def list_products_in_catalogue(id):
    catkey = cat_prod_key(id)
    return rc.zrange(catkey, 0, -1)

def search_product(name):
    results = rc.zscan(ALL_PRODUCTS, match="*%s*" % name.lower())
    response = []
    for result in results[1]:
        response.append(get_product(int(result[1])))
    return response

def add_product(name, description, vendor, price, main_catalogue_id, *images):
    if rc.exists(cat_key(main_catalogue_id)) == 0:
        raise ValueError("Catalogue id '%s' does not exist" % main_catalogue_id)
    
    id = get_counter(PRODUCT_COUNTER)
    pl = rc.pipeline(transaction=True)
    pl.hmset(prod_key(id), {"id": id, "name":name, "description":description, "vendor":vendor, "price":price, "main_catalogue_id": main_catalogue_id})
    image_list = []
    for image in images:
        image_idx = get_counter(IMAGE_COUNTER)
        pl.hmset(image_key(image_idx), {"id": image_idx, "value": image})
        image_list.append(image_key(image_idx))
        image_idx = image_idx + 1

    pl.sadd(prod_image_key(id), *image_list)
    pl.zadd(cat_prod_key(main_catalogue_id), {name: id})
    pl.zadd(ALL_PRODUCTS, {name.lower(): id})
    pl.execute()

    return get_product(id)

def delete_product(id):
    prodkey = prod_key(id)
    product = get_product(id)
    pl = rc.pipeline(transaction=True)
    pl.delete(prod_key(id), *product["images"], prod_image_key(id))
    pl.zremrangebyscore(cat_prod_key(int(product[b"main_catalogue_id"])), id, id)
    pl.zremrangebyscore(ALL_PRODUCTS, id, id)
    pl.execute()

def get_product(id):
    prodkey = prod_key(id)
    if rc.exists(prodkey) == 0:
        raise ValueError("Product with id '%s' does not exist" % id)
    product = rc.hgetall(prod_key(id))
    product["images"] = rc.smembers(prod_image_key(id))
    return product

def image_key(id):
    return "image:%s" % id

def prod_image_key(id):
    return "%s:images" % prod_key(id)
    
def prod_key(id):
    return "product:%s" % id

def cat_prod_key(id):
    return "%s:products" % cat_key(id)

def cat_key(id):
    return "catalogue:%s" % id
    
def get_counter(key):
    return rc.incr(key)

def get_product(id):
    prodkey = prod_key(id)
    if rc.exists(prodkey) == 0:
        raise ValueError("Product with id '%s' does not exist" % id)
    product = rc.hgetall(prod_key(id))
    product["images"] = rc.smembers(prod_image_key(id))
    return product

parser = argparse.ArgumentParser(description="Product Catalogue")
parser.add_argument('--add-catalogue', dest='addcat_set', action='store_true', help='add a catalogue')
parser.add_argument('--add-product', dest='addprod_set', action='store_true', help='add a product')
parser.add_argument('--delete-product', dest='delprod_set', action='store_true', help='delete a product')
parser.add_argument('--get-product', dest='getprod_set', action='store_true', help='get a product')
parser.add_argument('--search-product', dest='searchprod_set', action='store_true', help='search a product')
parser.add_argument('--list-by-category', dest='listbycat_set', action='store_true', help='list all products belonging to a category')
parser.add_argument('--id', dest='prod_id', help='id of product')
parser.add_argument('--name', dest='prod_name', help='name of product')
parser.add_argument('--description', dest='prod_desc', help='description of product')
parser.add_argument('--vendor', dest='prod_ven', help='vendor of product')
parser.add_argument('--price', dest='prod_price', help='price of product')
parser.add_argument('--catalogue', dest='prod_cat', help='catalogue of product')
parser.add_argument('--image', dest='prod_images', action='append', help="image.  can specify more than once")

args=parser.parse_args()

if args.addcat_set:
    print(create_catalogue(args.prod_name))
elif args.delprod_set:
    print(delete_product(args.prod_id))
elif args.addprod_set:
    print(add_product(args.prod_name, args.prod_desc, args.prod_ven, args.prod_price, args.prod_cat, *args.prod_images))
elif args.getprod_set:
    print(get_product(args.prod_id))
elif args.listbycat_set:
    print(list_products_in_catalogue(args.prod_id))
elif args.searchprod_set:
    print(search_product(args.prod_name))