# rl-product-catalogue

## Prerequisites
- Set the environment variables REDIS_HOST and REDIS_PORT for the redis host and port respectively
- Have python3 installed

## Examples
- Add a catalogue  
python3 pc.py --add-catalogue --name sample-catalogue

- Add a tshirt to a catalogue  
python3 pc.py --add-product --name TShirt --description 'This is a tee shirt' --vendor 'Tshirt vending vendor' --price 12.99 --catalogue 1 --image image1.png --image image2.png
  
- Add a trouser to a catalogue  
python3 pc.py --add-product --name Trousers --description 'This is a trouser' --vendor 'Same vendor that gave me a tshirt' --price 15.99 --catalogue 1 --image image3.png --image image4.png

- Add a product to a catalogue that doesn't exist.  You should get an error  
python3 pc.py --add-product --name TShirt --description 'This is a tee shirt' --vendor 'Tshirt vending vendor' --price 12.99 --catalogue 2 --image image1.png --image image2.png

- Get the product  
python3 pc.py --get-product --id 1

- List products that belong to a catalogue  
python3 pc.py --list-by-category --id 1

- Search for a product by name  (only return tshirt)  
python3 pc.py --search-product --name tshirt

- Search for a product by name  (return both tshirt and trousers)  
python3 pc.py --search-product --name t


- Delete the product  
python3 pc.py --delete-product --id 1
